package com.mycompany.app;

import org.junit.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;

public class HarmcrestTest
{
    @Test
    public void shouldCalculateWithRounding() throws Exception {
        BigDecimal actual = BigDecimal.valueOf(1.821).setScale(2, RoundingMode.HALF_UP);
        //JUnit assert
        assertEquals(BigDecimal.valueOf(1.82).setScale(2, RoundingMode.HALF_UP), actual);

        //Hamcrest matcher
        assertThat(actual,
                is(equalTo(BigDecimal.valueOf(1.82).setScale(2, RoundingMode.HALF_UP))));
    }
}
